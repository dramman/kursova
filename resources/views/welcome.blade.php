<link href="{{ asset('css/button.css') }}" rel="stylesheet">
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Bus station</title>

    </head>
    <body>
        <p>
        <a class="button" href="/trips">Start searching!</a>
        <a class="button" href="/admin/trips">Admin panel</a>
        <a class="button" href="/login">Login</a>
        <a class="button" href="/register">Registration</a>
    </body>
</html>
