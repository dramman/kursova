<html>
    <head>
        <title>Busstation "K-ON"</title>
        <link href="{{ asset('css/button.css') }}" rel="stylesheet">
    </head>
    <body>
        @yield('page_title')
        <br/><br/>
        <div class="container">
            Basic information
            @yield('content')
        </div>
        <br/><br/>
    </body>
    @include('app.layouts.footer')
</html>
