<link href="{{ asset('css/button.css') }}" rel="stylesheet">
<h1>Admin</h1>
<div style="float: left; width: 150px; height: 100%; margin-right: 30px;">
    <b>Trips</b>
    <ul>
        <li><a href="/admin/trips" class="button">List</a></li>
        <li><a href="/admin/trips/create" class="button">Add</a></li>
    </ul>
    <br/>
    <ul>
        <li><a href="/trips" class="button">Frontend</a></li>
    </ul>
</div>
<div>
    @yield('content')
</div>
