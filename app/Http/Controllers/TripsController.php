<?php

namespace App\Http\Controllers;

use App\Models\Trips;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TripsController extends Controller
{
    public function list(Request $request) {
        $dateFrom = $request->input('dateFrom', null);
        $dateTo = $request->input('dateTo', null);
        $dest = $request->input('dest', null);
        $inStock = $request->input('inStock', null);

        $model_trips = new Trips();
        $trips = $model_trips->getTrips($dateFrom, $dateTo, $dest, $inStock);
        $trip_cities = $model_trips->getCities();

        return view('app.trips.list', [
                'trips' => $trips,
                'trip_cities' => $trip_cities,
                'city_selected' => $dest,
                'date_from_selected' => $dateFrom,
                'date_to_selected' => $dateTo,
                'in_stock' => $inStock]
        );
    }

    public function trip($id, Request $request) {
        $book = $request->input('book', null);
        $model_trips = new Trips();
        $trip = $model_trips->getTripByID($id);

        if($book)
            if($trip->tickets+$book < 30)
                DB::table('trips')
                    ->where('trip_id', '=', $trip->trip_id)
                    ->update(['tickets' => $trip->tickets+$book]);
            else
                echo "Error! Not enough places.";

        return view('app.trips.trip', ['trip' => $trip]);
    }
}
