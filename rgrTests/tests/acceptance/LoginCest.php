<?php

use \Codeception\Step\Argument\PasswordArgument;

class LoginCest
{
   public function _before(AcceptanceTester $I)
   {
       $I->amOnPage('http://busstation.go');
       $I->click('Login');
       $I->fillField('email', 'artemzlobenko@gmail.com');
       $I->fillField('password', new PasswordArgument('77777777'));
   }

    public function tryLogin(FunctionalTester $I)
    {

        $I->click(['class' => 'btn']);
        $I->see('Travel list');
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
    }
}
